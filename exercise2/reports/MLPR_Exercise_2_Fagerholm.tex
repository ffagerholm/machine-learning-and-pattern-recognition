%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Short Sectioned Assignment
% LaTeX Template
% Version 1.0 (5/5/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[]{article}
\usepackage[a4paper, total={14cm, 24cm}]{geometry}

\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm} % Math packages

\usepackage{float}
\usepackage{graphicx} % For showing images 
\graphicspath{ {figures/} } % Images are in current folder

\usepackage{csvsimple} % for showing csv-file as table

\usepackage{listings} % Program listings
\usepackage{color}
 
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{commentblue}{rgb}{0.3,0.48,1}
\definecolor{stringred}{rgb}{0.7,0,0}
\definecolor{darkgreen}{rgb}{0,0.5,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

% Program listings style
\lstdefinestyle{mystyle}{
	backgroundcolor=\color{backcolour},
    commentstyle=\color{commentblue},
    keywordstyle=\color{darkgreen},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{stringred},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
 
\lstset{style=mystyle}

\numberwithin{equation}{section} % Number equations within sections
\numberwithin{figure}{section} % Number figures within sections
\numberwithin{table}{section} % Number tables within sections

%\setlength\parindent{0pt} % Removes all indentation from paragraphs - comment this line for an assignment with lots of text

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\title{	
\normalfont \normalsize 
\textsc{Turku University} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Machine Learning and Pattern Recognition \\ Exercise 2 \\ % The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}

\author{Fredrik Fagerholm, 608987}

\date{\normalsize\today}

\begin{document}

\maketitle % Print the title

\begin{abstract}
We develop and evaluate a deep learning model for facial recognition. A simple Convolution Neural Network (CNN) is implemented and evaluated on the \emph{extended Yale Face Database B} data set. We find that the model is well suited for this task, giving an estimated classification accuracy of $99.54\%$ in the test set.
\end{abstract}

\section{Facial recognition using deep learning}
%-----------------------------------------------

{\let\thefootnote\relax\footnote{{The report may be published on Moodle.}}}

\subsection{Data set}

The data set \cite{Lee:2005:ALS:1053556.1053705} consists of images of $38$ individuals. There are $64$ gray-scale images of each person and they have been taken in different lighting conditions. Some of the images have wrong dimensions, and are excluded. The images used have a height of $192$ pixels, and a width of $168$ pixels. There are a total of $2424$ images. The images are divided into separate directories for each of the $38$ persons. We use the directory name as the class label for the images. The labels are of the form \texttt{yaleBnn} where \texttt{nn = 01, 02, ..., 39} (N.B. no label \texttt{yaleB14}). 4 randomly chosen images from the data set are shown in figure \ref{fig:image_sample}.  

\begin{figure}[H]
    \includegraphics[width=\textwidth]{image_sample}
	\caption{A random sample from the image data set.}
	\label{fig:image_sample}
\end{figure}

The grayscale images are read and saved as $(192, 168, 1)$ matrices. The images are normalized so that the pixel values are between $0$ and $1$. The data set is split into a training and test set, with a 80:20 split. The split is stratified, meaning that there are an approximately equal number of representatives of each class in both the training and test sets.  

\subsection{Model}
We develop a classification model that is able to classify new images of the same individuals. 
The model used for the classification task is a Convolution Neural Network. The architecture consists of six layers, Two convolutional layers with max pooling layers in between, and two fully connected layers. A layout of the architecture is shown in figure \ref{fig:model_architecture}. Note that the input layer has the same dimensions as the image files, and the output layer has the same number of outputs as there are classes in the data set. A softmax function is used as the activation function for the output layer. The output of the softmax layer is a $38$-dimensional vector of values between $0$ and $1$, with norm $1$. An output is converted to a class label prediction by finding the position with the highest value.


\begin{figure}[H]
  	\centering
		\includegraphics[width=8cm]{model}
	\caption{Neural network architecture.}
	\label{fig:model_architecture}
\end{figure}

The labels are transformed to a 'one-hot' representation, that is, vectors of length $38$ where the $k$:th element is $1$ if the vector is the label for the $k$:th class. Since the labels are represented in this way and softmax is used as the activation function in the output layer, cross entropy can be  used as the optimization objective (loss function), the goal is to minimize this value.

\section{Experiment}
\subsection{Training} 
The model is trained with the images in the training set. We run it for $30$ epochs with a batch size of $32$. The loss and accuracy score is computed on the test set at the end of each training epoch. The value of the loss and accuracy on the training and test set at the end of each epoch is shown in  figure \ref{fig:validation_curve}. The \emph{Adam}  algorithm is used for optimization, as it has been shown to be effective for CNN models  \cite{DBLP:journals/corr/KingmaB14, DBLP:journals/corr/Ruder16}. 

\begin{figure}[H]
  	\centering
		\includegraphics[width=\textwidth]{validation_curve}
	\caption{Validation curve. The loss and accuracy in the training and test sets at the end of each epoch.}
	\label{fig:validation_curve}
\end{figure}

We see that the loss does not decrease significantly after the fifth epoch, so the training could probably have been stopped early and we would have gotten almost as good performance. There is a sharp increase in the accuracy, both in the training and test set, for the first few epochs but no significant increase after that. 


\subsection{Evaluation}
The trained model is evaluated on the test set. We use the model to generate predictions for the images in the test set. The prediction are then compared to the true labels and accuracy score is computed for each class. The confusion matrix is shown in figure \ref{fig:confusion_matrix}. We see that the performs very well on the test set, reaching a accuracy of $99.54\%$. The confusion matrix also shows that there are very few misclassification. 

\subsection{Classification}

\begin{figure}[H]
  	\centering
		\includegraphics[width=10cm]{confusion_matrix}
	\caption{Confusion matrix for the predictions.}
	\label{fig:confusion_matrix}
\end{figure}

There are in fact only $4$ misclassifications in total, for the classes 'yaleB35' 'yaleB15' and  'yaleB36'. These misclassified images are shown in figure \ref{fig:wrong_predictions} along with the probabilities that the model gave to the predictions. We see that the images all have a lot of shadows, and some kind of digital artifacts. These images would probably be hard to classify even for a human. \footnote{Interesting to note that the eigenfaces model we used in the previous exercise did also make classification errors on classes 'yaleB35' and 'yaleB36', but not 'yaleB15'. Still, this could be due to randomness in sampling for the test set.} An interesting observation is that the model gives a very high class probability ($1.0$) to the misclassified images. We should still be careful about how to interpret these probabilities \cite{DBLP:journals/corr/NguyenYC14, Gal2016Uncertainty}

\begin{figure}[H]
  	\centering
		\includegraphics[width=10cm]{wrong_predictions}
	\caption{Random sample of failed classifications.}
	\label{fig:wrong_predictions}
\end{figure}

%----------------------------------------------------------------------------------------
\bibliographystyle{plain}
\bibliography{references}

%----------------------------------------------------------------------------------------
% Appedices
\pagebreak

\section{Appendix A: Requirements} 
\texttt{Python version: 3.6}
\lstinputlisting{../requirements.txt}

\pagebreak
\section{Appendix B: Source code}
\lstinputlisting[language=Python]{../notebooks/MLPR_Exercise_2_Fagerholm.py}

\end{document}