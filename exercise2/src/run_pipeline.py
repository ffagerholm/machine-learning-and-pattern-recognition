from data.process_image_data import read_image_data
from data.generate_data import generate_dummy_data
from data.split_data import split_train_test
from models.cnn_model import train_cnn_model, test_cnn_model
from ruffus import pipeline_run, pipeline_printout_graph


if __name__ == '__main__':
    pipeline_run([train_cnn_model])