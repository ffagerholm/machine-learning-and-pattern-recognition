# -*- coding: utf-8 -*-
import numpy as np 
from scipy.special import expit
from ruffus import originate


@originate('data/processed/dummy_data.npz')
def generate_dummy_data(output_file):
    n_samples = 10000
    n_features = 20


    cov = np.random.uniform(0.0, 0.1, size=(n_features, n_features))

    features = np.random.multivariate_normal(mean=np.zeros(shape=(n_features,)), 
                                             cov=cov,
                                             size=(n_samples,))
    weights = np.random.uniform(size=(n_features, 1))
    noise = np.random.normal(loc=0.0, scale=7, size=(n_samples, 1))
    labels = (expit(np.dot(features, weights) + noise) > 0.5).astype(int)

    # save data
    np.savez(output_file, features=features, labels=labels)
