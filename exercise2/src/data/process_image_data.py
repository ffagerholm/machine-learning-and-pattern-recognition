# -*- coding: utf-8 -*-
import os
import zipfile
import fnmatch
import logging
import numpy as np
import imageio
from ruffus import pipeline_run, files

log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)



@files('data/raw/CroppedYale.zip', 'data/processed/image_data.npz')
def read_image_data(input_file, output_file, 
                    file_format='*.pgm', img_dim=(192, 168), n_samples=2424):
    """ 
    Runs data processing scripts to turn raw image data from (../raw) into
    cleaned data in form of NumPy arrays (saved in ../processed).
    """    
    logger = logging.getLogger(__name__)
    logger.info('making data set from raw image data')

    img_height, img_width = img_dim
    
    image_data = np.empty(shape=(n_samples, img_height, img_width, 1), dtype='float32')
    labels = np.empty(shape=(n_samples, 1), dtype='<S10')

    logger.info('reading image data from: {}'.format(input_file))

    index = 0
    # read image data and save as vectors
    # open compressed data directory
    with zipfile.ZipFile(input_file, 'r') as z:
        for filename in fnmatch.filter(z.namelist(), file_format):
            # extract file
            infile = z.extract(filename)
            # read image as grayscale
            image = imageio.imread(infile)
            # check that the image has correct dimensions
            if image.shape == img_dim:
                # save image vector
                image_data[index, :, :, :] = image.reshape((img_height, img_width, 1))
                # use directory name as label for sample
                labels[index, :] = os.path.dirname(filename).split('/')[1]
                index += 1
            else:
                logger.error('wrong dimensions: {}, image not saved'.format(filename))

    # normalize image data to range [0, 1]
    image_data /= 255.0

    # count unique class names
    n_classes = np.unique(labels).shape[0]

    logger.info('{} images read'.format(index))
    logger.info('{} unique labels'.format(n_classes))
    
    # save data
    np.savez(output_file, features=image_data, labels=labels)
