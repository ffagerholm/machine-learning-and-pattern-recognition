# -*- coding: utf-8 -*-
import logging
import numpy as np
from sklearn.model_selection import train_test_split
from ruffus import subdivide, formatter
from .process_image_data import read_image_data
from .generate_data import generate_dummy_data

log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)


@subdivide( [read_image_data, generate_dummy_data],
            formatter(),
            "{subpath[0]}/{basename[0]}.npz",
            "{subpath[0][0]}/{basename[0]}")
def split_train_test(input_file, output_files, output_file_name_root):
    """
    split data set into training and test set
    """
    logging.info("input_file: {}".format(input_file))
    logging.info("output_files: {}".format(output_files))
    logging.info("output_file_name_root: {}".format(output_file_name_root))

    with np.load(input_file) as data:
        X = data['features']
        y = data['labels'].ravel()

    X_train, X_test, y_train, y_test = train_test_split(X, y, shuffle=True,
                                            test_size=0.20, stratify=y)

    np.savez(output_file_name_root + '_train.npz', X_train=X_train, y_train=y_train) 
    np.savez(output_file_name_root + '_test.npz', X_test=X_test, y_test=y_test)
