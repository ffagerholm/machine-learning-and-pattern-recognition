# -*- coding: utf-8 -*-
import os
import logging
import numpy as np
from ruffus import pipeline_run, files, follows
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras import backend as K
from keras.optimizers import SGD

from visualization.plot_learning import PlotLearning
from data.split_data import split_train_test

log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)


@follows(split_train_test)
@files('data/processed/dummy_data_train.npz', 'models/simple_model.hdf5')
def train_model(input_filepath, output_filepath):
    # read training data from file
    with np.load(input_filepath) as data:
        X_train = data['X_train']
        y_train = data['y_train'].ravel()
        X_test = data['X_train'] 
        y_test = data['y_train'].ravel()

    num_features = X_train.shape[1]
    num_classes = np.unique(y_train).shape[0]

    y_train = keras.utils.to_categorical(y_train, num_classes=num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes=num_classes)

    model = Sequential()
    model.add(Dense(32, activation='relu', input_dim=num_features))
    model.add(Dense(num_classes, activation='softmax'))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])


    logging.info('training simple model')
    plot_learning = PlotLearning()
    history = model.fit(X_train, y_train, 
                        batch_size=32, epochs=30,
                        validation_data=(X_test, y_test),
                        callbacks=[plot_learning])

    logging.info('saving model to: {}'.format(output_filepath))
    model.save(output_filepath)


@follows(train_model)
@files('data/processed/dummy_data_test.npz', 
       'models/results_dummy.dat', 
       'models/simple_model.hdf5')
def test_model(input_file, output_file, model_filepath):
    with np.load(input_file) as data:
        X_test = data['X_test']
        y_test = data['y_test'].ravel()

    y_test = keras.utils.to_categorical(y_test, num_classes=2)
    model = keras.models.load_model(model_filepath)
    score = model.evaluate(X_test, y_test)

    with open(output_file, 'w') as outfile:
        outfile.write('{},{}\n'.format(model.loss, model.metrics[0]))
        outfile.write(','.join(map(str, score)) + '\n')
