# -*- coding: utf-8 -*-
import logging
import numpy as np
from ruffus import pipeline_run, files, follows
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD
from keras import backend as K
from sklearn.preprocessing import LabelEncoder
from data.split_data import split_train_test

log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)


@follows(split_train_test)
@files('data/processed/image_data_train.npz', 'models/cnn_model.hdf5')
def train_cnn_model(input_filepath, output_filepath):
    # read training data from file
    with np.load(input_filepath) as data:
        X_train = data['X_train']
        y_train = data['y_train'].ravel()
        X_test = data['X_train'] 
        y_test = data['y_train'].ravel()

    # transform labels to binary representation
    y_train = LabelEncoder().fit_transform(y_train)
    num_classes = np.unique(y_train).shape[0]
    y_train = keras.utils.to_categorical(y_train, num_classes=num_classes)

    model = Sequential()
    # input: 192x168 images with 1 channel -> (192, 168, 1) tensors.
    model.add(Conv2D(16, (5, 5), activation='relu', input_shape=(192, 168, 1)))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(36, (5, 5), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='softmax'))


    #sgd = SGD(lr=0.05, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', 
                  optimizer=keras.optimizers.Adadelta(), 
                  metrics=['accuracy'])

    logging.info('training CNN model')
    history = model.fit(X_train, y_train, 
                        batch_size=32, epochs=3,
                        validation_data=(X_test, y_test),
                        callbacks=[plot_learning])

    logging.info('saving model to: {}'.format(output_filepath))
    model.save(output_filepath)


@follows(train_cnn_model)
@files('data/processed/image_data_test.npz', 'models/results.dat', 'models/cnn_model.hdf5')
def test_cnn_model(input_file, output_file, model_filepath):
    
    with np.load(input_file) as data:
        X_test = data['X_test']
        y_test = data['y_test'].ravel()

    y_test = LabelEncoder().fit_transform(y_test)
    num_classes = np.unique(y_test).shape[0]
    y_test = keras.utils.to_categorical(y_test, num_classes=num_classes)

    model = keras.models.load_model(model_filepath)

    score = model.evaluate(X_test, y_test)
    
    with open(output_file, 'w') as outfile:
        outfile.write('{},{}\n'.format(model.loss, model.metrics[0]))
        outfile.write(','.join(map(str, score)) + '\n')
