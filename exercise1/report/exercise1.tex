%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Short Sectioned Assignment
% LaTeX Template
% Version 1.0 (5/5/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[]{article}
\usepackage[a4paper, total={14cm, 24cm}]{geometry}

\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm} % Math packages

\usepackage{float}
\usepackage{graphicx} % For showing images 
\graphicspath{ {.} } % Images are in current folder

\usepackage{csvsimple} % for showing csv-file as table

\usepackage{listings} % Program listings
\usepackage{color}
 
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{commentblue}{rgb}{0.3,0.48,1}
\definecolor{stringred}{rgb}{0.7,0,0}
\definecolor{darkgreen}{rgb}{0,0.5,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

% Program listings style
\lstdefinestyle{mystyle}{
	backgroundcolor=\color{backcolour},
    commentstyle=\color{commentblue},
    keywordstyle=\color{darkgreen},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{stringred},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
 
\lstset{style=mystyle}

\numberwithin{equation}{section} % Number equations within sections
\numberwithin{figure}{section} % Number figures within sections
\numberwithin{table}{section} % Number tables within sections

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\title{	
\normalfont \normalsize 
\textsc{Turku University} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Machine Learning and Pattern Recognition \\ Exercise 1 \\ % The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}

\author{Fredrik Fagerholm, 608987}

\date{\normalsize\today}

\begin{document}

\maketitle % Print the title

\begin{abstract}
We implement the Eigenfaces technique for facial recognition, by using Principal Component Analysis in combination with a K-nearest neighbors classifier. Cross-validation yields an estimated F1 score of $0.92$ on a cropped version of the Yale Face B data set. We conclude that the technique works quite well for this data set.
\end{abstract}

{\let\thefootnote\relax\footnote{{The report may be published on the Moodle page}}}

\section{Recognition of human faces}

In this report we investigate how the eigenfaces algorithm can be used for facial recognition. Eigenfaces has been chosen as the technique to be used, because of its popularity for this application\cite{Chihaoui2016ASO}, and it providing reasonably good performance. 

%------------------------------------------------

\section{Eigenfaces}

The main idea behind the eigenfaces approach is that the high dimensional image-vectors can be represented with some loss of accuracy in a space with lower dimensionality \cite{Hiremath2009FaceRU}. 

This lower dimensional space is obtained by computing the principal components of the data matrix. The principal components are the eigenvectors of the covariance matrix of the data, i.e. images of faces, hence the name eigenfaces. Any face from the data set can be represented by a linear combination of the full set of eigenvectors. But any face can also be represented by a subset of the eigenvectors, with some loss of accuracy. Most of the information contained in the images can usually be preserved in the space spanned by a subset of the eigenvectors, a so called face space. 

Classification of a new image is done by projecting the image onto the face space and comparing its position to the known samples in the projected training set, and using some classification algorithm to generate a prediction. In this report we use K-nearest neighbors as the classification algorithm.

\section{Experiment} 

\subsection{Data set}

The data set used is the Extended Yale Face Database B \cite{ExtYaleB}. It consists of images of $38$ individuals. There are $64$ gray-scale images of each person and they have been taken in different lighting conditions. Some images are excluded due to them having different dimension than the majority of the images. We use a total of $2424$ images. Each image has a height of $192$ pixels, and a width of $168$ pixels. Figure \ref{fig:image_sample} shows 4 randomly chosen images from the data set.

\begin{figure}[H]
    \includegraphics[width=\textwidth]{image_sample}
	\caption{A random sample from the image data set.}
	\label{fig:image_sample}
\end{figure}


The images are read from the files, reshaped and stored as vectors of length $192\cdot168 = 32256$. This results in a data matrix of shape $(2424, 32256)$. The label for each sample is taken from the folder that the images of that individual was stored in. The labels are of the form \texttt{yaleBnn} where \texttt{nn = 01, 02, ..., 39} (N.B. no label \texttt{yaleB14}).

\subsection{Feature extraction and preprocessing}

The data is split into a training set and a test set, with $25\%$ of the data left for testing. The sampling is stratified, meaning that the frequency of classes in both the training and test set are (almost) evenly distributed as shown in figure \ref{fig:class_distribution}.

\begin{figure}[H]
  	\centering
		\includegraphics[width=\textwidth]{class_distribution}
	\caption{Class distribution in training and test sets.}
	\label{fig:class_distribution}
\end{figure}

We compute the $100$ first principal components using the training set and project both the train and test data onto the subspace spanned by the principal components. This number was chosen arbitrarily.

The cumulative variance explained by including consecutive principal components, as well as the ratio of the variance explained by each of the $10$ first principal components is shown in figure \ref{fig:principal_components}. We see that a large portion of the variance is explained by the first few principal components.

\begin{figure}[H]
  	\centering
		\includegraphics[width=\textwidth]{principal_components}
	\caption{Variance explained by the 10 first principal components.}
	\label{fig:principal_components}
\end{figure}

The eigenfaces used in our model are the $100$ first principal components. The first $4$ eigenfaces are shown in figure \ref{fig:eigenfaces}.

\begin{figure}[H]
  	\centering
		\includegraphics[width=\textwidth]{eigenfaces}
	\caption{4 first eigenfaces computed form the training set.}
	\label{fig:eigenfaces}
\end{figure}

\subsection{Classification}

We use a K-nearest neighbors model as the classifier. The transformed training set is used to tune the hyper-parameter $K$, through $5$-fold cross-validation. Values $K = 1, \ldots, 5$ are tested, and we find that $K = 1$ gives the best performance.

We use the $1$-nearest neighbor model on the transformed test set to generate predictions. We obtain a F1 score (using macro averaging) of $0.92$, see table \ref{table:classification_report} in appendix A. 
The confusion matrix for the predictions is shown in figure \ref{fig:confusion_matrix}. Most of the individual classes seem to have an accuracy over $0.80$, and there are quite few false positives 
and false negatives. This is confirmed by the results displayed in table \ref{table:classification_report}.

\begin{figure}[H]
  	\centering
		\includegraphics[width=10cm]{confusion_matrix}
	\caption{Confusion matrix for the predictions.}
	\label{fig:confusion_matrix}
\end{figure}

Figure \ref{fig:wrong_predictions} shows a random sample of failed classifications. Based on the images shown it seems that the classifier makes more errors for darker images, or images with a lot of shadows. 
There is also an image with some kind of digital noise, in the erroneous classified images.

\begin{figure}[H]
  	\centering
		\includegraphics[width=\textwidth]{wrong_predictions}
	\caption{Random sample of failed classifications.}
	\label{fig:wrong_predictions}
\end{figure}

\section{Conclusions}
The results from the experiment suggests that the eigenfaces technique is quite effective for this particular data set\footnote{We also tested the Linear Discriminant Analysis algorithm on this data and got an even higher F1 score of $0.97$. Eigenfaces was still chosen for the report because we found it more interesting and was easier to explain.}. It is interesting to note that some of images that the  classifier gave wrong predictions for would presumably also be quite hard for a human to recognize, especially the examples of \texttt{yaleB35},\texttt{yaleB16} and \texttt{yaleB24} in figure \ref{fig:wrong_predictions}.

Still, we have to keep in mind that these images have been taken for the specific purpose of facial recognition. All images are taken in a the same setting, the faces are close to the center in most images, and all images are cropped in the same way. In a more realistic setting this method would probably fare worse, and preprocessing of the images (facial detection, centering, cropping, etc.) would  be needed. 


%----------------------------------------------------------------------------------------
\bibliographystyle{plain}
\bibliography{references}

%----------------------------------------------------------------------------------------
% Appedices
\pagebreak

\section{Appendix A: Supplementary tables and figures} 

\begin{table}[H]
	\small
	\centering
	\csvautotabular{classification_report.csv}
	\caption{Classification report. Precision, recall and f1-score
	for each of the classes in the test set, and number of samples of that class. 
	The last row shows the averages of the metrics, and the total number of 
	samples in the test set.}
	\label{table:classification_report}
\end{table}

\pagebreak
\section{Appendix B: Source code}
\lstinputlisting[language=Python]{../Ex1_Fagerholm.py}

\end{document}